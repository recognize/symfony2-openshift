# README #

Use this repository to create a skeleton Symfony 2.4.4 project, ready to be deployed to Openshift.

## Usage
* Create an application in Openshift and use the HTTPS URL of this repository during deployment
* Don't forget to add a MySQL container!
* Fork this repository for every project that you want to use
* Clone the forked repository
* Add the Openshift repository as a remote (git remote add <name> <url>
* When pushing to the openshift repository, the application will automatically restart the various services

## What's been altered in the Symfony installation?
* app/config/config.yml (added resource params.php)
* app/config/params.php (added this file to set Openshift database user/pass/host)
* .htaccess (added root .htaccess file to redirect to the /web directory)
